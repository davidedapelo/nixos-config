"""""""""""""""""""" User-defined settings """"""""""""""""""""
"Prevent backup-.swp files from spawning
set nobackup
set noswapfile

"Highlighting the current line
set cursorline

"Updating time
set updatetime=100

"Tabbing, synthax and spacing
syntax on
filetype plugin indent on

"set indentation for C/C++, xml and bash files
autocmd FileType cpp,c,cxx,h,hpp,hh,sh,xml setlocal tabstop=2
autocmd FileType cpp,c,cxx,h,hpp,hh,sh,xml setlocal shiftwidth=2
autocmd FileType cpp,c,cxx,h,hpp,hh,sh,xml setlocal expandtab

"disable any indentation for LaTeX files
autocmd FileType tex setlocal noautoindent
autocmd FileType tex setlocal nocindent
autocmd FileType tex setlocal nosmartindent
autocmd FileType tex setlocal indentexpr=

"Set Citrl-X/C/V
vmap <C-c> "+yi
vmap <C-x> "+c
vmap <C-v> c<ESC>"+p
imap <C-v> <C-r><C-o>+

"Set number & Toggle relative line number with Ctrl-L
"set nuw=6
set number
nmap <C-L> :set invrelativenumber<CR>

"Enable gruvbox theme
colorscheme gruvbox
syntax enable
set background=dark

"Set opacity
autocmd VimEnter * silent !xprop -f _NET_WM_WINDOW_OPACITY 32c -set _NET_WM_WINDOW_OPACITY `echo $((0xffffffff * 95 / 100))` -id $(xprop -root 32x '\t$0' _NET_ACTIVE_WINDOW | cut -f 2)

"Start maximized
autocmd VimEnter * silent !wmctrl -r :ACTIVE: -b add,maximized_vert,maximized_horz

"Buffergator
let g:buffergator_viewport_split_policy="B"

"Nerdtree opening at startup (and Tagbar)
nmap ,n :NERDTreeFind<CR>
nmap ,m :NERDTreeToggle<CR>
nmap ,t :TagbarToggle<CR>
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
autocmd vimenter * NERDTree
"autocmd vimenter * NERDTree | TagbarOpen "also opening Tagbar

"Tagbar
autocmd BufEnter * nested :call tagbar#autoopen(0)

"Copy outside vim (to be tested
set mouse=a
