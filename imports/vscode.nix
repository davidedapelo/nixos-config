{ pkgs, lib, ... }:

{
  #environment.systemPackages = with pkgs.unstable; [
  environment.systemPackages = with pkgs; [
    (vscode-with-extensions.override {
      # When the extension is already available in the default extensions set.
      vscodeExtensions = with vscode-extensions; [
        bbenoist.nix
        dotjoshjohnson.xml
        eamodio.gitlens
        james-yu.latex-workshop
        mads-hartmann.bash-ide-vscode
        mhutchie.git-graph
        ms-ceintl.vscode-language-pack-it
        ms-python.python
        ms-python.vscode-pylance
        ms-vscode.cpptools
        vscodevim.vim
      ]
      # Concise version from the vscode market place when not available in the default set.
      ++ vscode-utils.extensionsFromVscodeMarketplace [
        {
          name = "cpptools-extension-pack";
          publisher = "ms-vscode";
          version = "1.3.0";
          sha256 = "sha256:rHST7CYCVins3fqXC+FYiS5Xgcjmi7QW7M4yFrUR04U=";
        }
        {
          name = "doxdocgen";
          publisher = "cschlosser";
          version = "1.4.0";
          sha256 = "sha256:InEfF1X7AgtsV47h8WWq5DZh6k/wxYhl2r/pLZz9JbU=";
        }
        {
          name = "glassit";
          publisher = "s-nlf-fh";
          version = "0.2.6";
          sha256 = "sha256-LcAomgK91hnJWqAW4I0FAgTOwr8Kwv7ZhvGCgkokKuY=";
        }
        {
          name = "gruvbox-concoctis";
          publisher = "wheredoesyourmindgo";
          version = "10.30.27";    
          sha256 = "sha256:crjpGzJqjfVWcuO4kaGqtrs4Izhvh1QlWU7uurteLl8=";
        }
        {
          name = "vscode-double-line-numbers";
          publisher = "slhsxcmy";
          version = "0.1.3";    
          sha256 = "sha256:sU69uuKUpc7JTo5h/m4faGgUVP4EnVUHisxF+EAEvl8=";
        }
        {
          name = "makefile-tools";
          publisher = "ms-vscode";
          version = "0.6.0";    
          sha256 = "sha256:Sd1bLdRBdLVK8y09wL/CJF+/kThPTH8MHw2mFQt+6h8=";
        }
      ];
    })
  ];
}
