{ config, pkgs, ... }:

let
  # Home-manager channel in a declarative way
  home-manager = builtins.fetchTarball "https://github.com/nix-community/home-manager/archive/release-22.05.tar.gz";
in
{
  imports =
    [ # Include the results of the hardware scan.
      (import "${home-manager}/nixos")
    ];

  # home-manager setup
  home-manager.users.davide = {

    # Example of key binding,  for future reference
    #dconf.settings = {
    #  "org/gnome/settings-daemon/plugins/media-keys" = {
    #    custom-keybindings = [
    #      "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/"
    #    ];
    #  };
    #  "org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0" = {
    #    binding = "<Alt>1";
    #    command = "bash /home/davide/.config/launch_terminator";
    #    name = "open-terminal";
    #  };
    #} ;

    # Allow unfree packages - needs to be repeated within home-manager
    nixpkgs.config.allowUnfree = true;

    programs.fzf.enable = true;
    programs.fzf.enableZshIntegration = true;

    programs.git = {
      enable = true;
      package = pkgs.gitAndTools.gitFull;
      includes = [
        { path = ../dotfiles/.gitconfig; }
      ];
      extraConfig = {
        credential.helper = "libsecret";
      };
    };

    programs.vim = {
      enable = true;
      plugins = with pkgs.vimPlugins; [
        gruvbox
        nerdtree
        nerdtree-git-plugin
        tagbar
        vim-airline
        vim-gitgutter
        vim-fugitive
        vim-merginal
        vim-buffergator
      ];
      extraConfig = builtins.readFile ../dotfiles/.vimrc;
    };

    programs.zsh = {
      enable = true;
      enableAutosuggestions = true;
      enableSyntaxHighlighting = true;
      oh-my-zsh = {
        enable = true;
        plugins = [
          "git"
        ];
        theme = "robbyrussell";      
      };
    };

    services.fusuma = {
      enable = true;
      extraPackages = with pkgs; [ xdotool ];
      settings = {
        threshold = { swipe = 0.1; };
        interval = { swipe = 0.7; };
        swipe = {
          "3" = {
            left = {
              # GNOME: Switch to left workspace
              command = "xdotool key ctrl+alt+Left";
            };
            right = {
              # GNOME: Switch to right workspace
              command = "xdotool key ctrl+alt+Right";
            };
          };
        };
      };
    };

  };
}
