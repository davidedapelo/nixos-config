{ config, pkgs, ... }:

{
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.davide = {
    isNormalUser = true;
    description = "Davide";
    extraGroups = [ "networkmanager" "wheel" ];
    packages = with pkgs; [
      # script as a package
      (writeShellScriptBin "idprocessi.sh" ''
        wmctrl -l;
      '')
      (writeShellScriptBin "transparent.sh" ''
        xprop -id $1 -f _NET_WM_WINDOW_OPACITY 32c -set _NET_WM_WINDOW_OPACITY $(printf 0x%x $((0xffffffff * $2 / 100)));
      '')
    ];
  };
}