{ config, pkgs, ... }:

{
  # HiDPI (Retina Display in apple)
  hardware.video.hidpi.enable = true;

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Enable the GNOME Desktop Environment.
  services.xserver.displayManager.gdm.enable = true;
  services.xserver.desktopManager.gnome.enable = true;

  # Configure keymap in X11
  services.xserver = {
    layout = "gb";
    xkbVariant = "intl";
  };
  # gnome extensions (apparently)
  services.udev.packages = with pkgs; [ gnome.gnome-settings-daemon ];
}