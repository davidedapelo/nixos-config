{ config, pkgs, ... }:

let
  # Unstable packages
  unstableTarball = fetchTarball https://github.com/NixOS/nixpkgs/archive/nixos-unstable.tar.gz;

  # Local clone of the nixpkgs repository
  #localNixpkgs = ../../nixpkgs;

  # Custom Python package with all the (Python) imports I need
  my-python-packages = python-packages: with python-packages; [
#    (
#      buildPythonPackage rec {
#        pname = "tensorflow";
#        version = "2.13.0";
#        src = fetchPypi {
#          inherit pname version;
#          sha256 = "sha256-AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=";
#        };
#        doCheck = false;
#        propagatedBuildInputs = [
#          # Specify dependencies
#                  pkgs.python3Packages.numpy
#        pkgs.python3Packages.scipy
#        pkgs.python3Packages.six
#        pkgs.python3Packages.wheel
#        ];
#      }
#    )
    matplotlib
    numpy
    openpyxl
    pandas
    scipy
    tensorflow
    keras
  ]; 
  #python-with-my-packages = pkgs.unstable.python311.withPackages my-python-packages;
  #python-with-my-packages = pkgs.local.python3.withPackages my-python-packages;
  python-with-my-packages = pkgs.unstable.python3.withPackages my-python-packages;
  #python-with-my-packages = pkgs.python3.withPackages my-python-packages;
in
{
  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # Overlays for unstable packages and for patches
  nixpkgs.overlays = [
    # Overlay to define unstable packages
    (
      final: prev: {
        unstable = import unstableTarball {
          config = config.nixpkgs.config;
        };
      }
    )
    # Overlay to define packages from local clone
#    (
#      final: prev: {
#        local = import localNixpkgs {
#          config = config.nixpkgs.config;
#        };
#      }
#    )
    # Template for an overlay to define a patch
    #(
    #  self: super: {
    #    # see https://github.com/NixOS/nixpkgs/pull/206745
    #    clisp = super.clisp.override {
    #      readline = pkgs.readline6;
    #    };
    #  }
    #)
  ];

  # to try making tensorflow work
  hardware.opengl.setLdLibraryPath = true;
  
  # Once needed this hack to install tensorflow despite being marked as insecure.
  # Now tensorflow no longer needs this, but keeping the code commented as an example.
  #nixpkgs.config.permittedInsecurePackages = [
  #  "python3.10-tensorflow-2.11.0"
  #  "tensorflow-2.11.0"
  #  "tensorflow-2.11.0-deps.tar.gz"
  #];


  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    audacious
    bc
    binutils
    clapper
    cmake # for petsc
    ctags
    ffmpeg
    firefox
    fusuma
    gcc
    gfortran # for petsc
    gimp
    git
    gnome.dconf-editor
    gnome.gnome-tweaks
    gnome.sushi
    gnomeExtensions.appindicator
    gnomeExtensions.bluetooth-quick-connect
    gnomeExtensions.ddterm
    gnomeExtensions.pip-on-top
    gnumake
    gnuplot
    unstable.google-chrome
    imagemagick
    inkscape
    libreoffice
    mendeley
    mpi
    nix-index
    onlyoffice-bin
    openvpn
    paraview
    pcloud
    pciutils
    poppler_utils
    python-with-my-packages
    qbittorrent
    ruby
    steam-run
    texlive.combined.scheme-full
    tor-browser-bundle-bin
    update-resolv-conf
    vimHugeX
    viu
    vlc
    wget
    wmctrl
    xdotool
    xorg.xev # just for testing; not really useful otherwise
    zoom
    zotero
  ];
}
