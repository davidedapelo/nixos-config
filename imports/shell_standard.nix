{ config, pkgs, ... }:

{
  # Configure console keymap
  console.keyMap = "uk";

  # The shell is set to be zsh.
  # zsh setting must be declared in home manager!

  # Default shell for all users
  users.defaultUserShell = pkgs.zsh;

  # Recommended for some programs
  environment.shells = with pkgs; [  zsh ];
}