{ config, pkgs, ... }:

let
  # Custom Python package with all the (Python) imports I need
  my-python-packages = python-packages: with python-packages; [
    matplotlib
    numpy
    openpyxl
    pandas
    scipy
  ]; 
  python-with-my-packages = pkgs.python3.withPackages my-python-packages;
in
{
  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    bc
    binutils
    ctags
    gcc
    git
    gnumake
    gnuplot
    imagemagick
    lsof
    mpi
    python-with-my-packages
    vlc
    wget
  ];
}
