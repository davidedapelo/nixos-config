{ config, pkgs, ... }:

{
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.admin = {
    isNormalUser = true;
    description = "admin";
    extraGroups = [ "networkmanager" "wheel" "vboxsf" ];
    packages = with pkgs; [];
  };
  users.users.user = {
    isNormalUser = true;
    description = "user";
    extraGroups = [ "vboxsf" ];
    packages = with pkgs; [];
  };
}